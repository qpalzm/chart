package ru.chart;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.Producer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.chart.consumer.ConsumerCreator;
import ru.chart.producer.ProducerCreator;

@Configuration
@ComponentScan
public class App {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(App.class);
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Chart");
        System.out.println("Enter login:");
        String login = scanner.nextLine();
        if (login.isEmpty()) System.exit(-1);
        ConsumerCreator consumerCreator = context.getBean(ConsumerCreator.class);
        consumerCreator.runConsumer(login);
        ProducerCreator producerCreator = context.getBean(ProducerCreator.class);
        Producer<Long, String> producer = producerCreator.getProducer(login);
        System.out.println("For send message to all - all");
        System.out.println("For send message private  - pv");
        String command = "";

        while (!"exit".equals(command)){
            command = scanner.nextLine();
            if ("all".equals(command)){
                System.out.println("Enter message: ");
                String message  = scanner.nextLine();
                message = login.concat(": ").concat(message);
                producerCreator.sendMessageBroadcast(producer, message);
            } else if ("pv".equals(command)){
                System.out.println("Enter message: ");
                String message  = scanner.nextLine();
                message = login.concat(" (p):").concat(message);
                System.out.println("Enter login: ");
                final String topic  = scanner.nextLine();
                producerCreator.sendMessagePrivate(producer, message, topic);
            }
        }
    }

}