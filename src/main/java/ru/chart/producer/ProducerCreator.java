package ru.chart.producer;

import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;
import ru.chart.share.KafkaConstants;

@Component
public class ProducerCreator {

    private Producer<Long, String> createProducer(String login) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConstants.KAFKA_BROKERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, login);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    public Producer<Long, String> getProducer(String login) {
        return createProducer(login);
/*        for (int index = 0; index < KafkaConstants.MESSAGE_COUNT; index++) {
            ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(KafkaConstants.TOPIC_NAME,
                                                                                   "This is record " + index);
            try {
                RecordMetadata metadata = producer.send(record).get();
                System.out.println("Record sent with key " + index + " to partition " + metadata.partition()
                                           + " with offset " + metadata.offset());
            } catch (ExecutionException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            } catch (InterruptedException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            }
        }*/
    }

    public void sendMessageBroadcast(Producer<Long, String> producer, String message) {
        try {
            ProducerRecord<Long, String> record = new ProducerRecord<>(KafkaConstants.TOPIC_NAME, message);
            producer.send(record).get();
        } catch (Exception e) {
            System.out.println("Что-то пошло не так");
            System.out.println(e.getMessage());
        }
    }

    public void sendMessagePrivate(Producer<Long, String> producer, String message, String topic) {
        try {
            ProducerRecord<Long, String> record = new ProducerRecord<>(topic, message);
            producer.send(record).get();
        } catch (Exception e) {
            System.out.println("Что-то пошло не так");
            System.out.println(e.getMessage());
        }

    }

}
